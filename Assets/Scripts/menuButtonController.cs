using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuButtonController : MonoBehaviour
{
    public int index;
    [SerializeField] bool keyDown;
    [SerializeField] int maxIndex;
    public AudioSource audioSouce;
    // Start is called before the first frame update
    void Start()
    {
        audioSouce = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetAxis("Vertical") != 0){//
            if(!keyDown){
                if(Input.GetAxis("Vertical") < 0)
                {
                    if(index < maxIndex){//loop if u go to the buttom you have to go back to the top
                        index++;
                    }else{
                        index = 0;
                    }
                    

                    
                }
                else if (Input.GetAxis ("Vertical")> 0){//go back
                    if (index > 0)
                    {
                        index --;
                    }
                    else{
                        index = maxIndex;
                    }
                }
                keyDown = true;
            }
        }else{
            keyDown = false;
        }
        
    }
}
