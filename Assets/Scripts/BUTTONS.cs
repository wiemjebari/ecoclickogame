using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BUTTONS : MonoBehaviour
{
    public string firstlevel;
    public GameObject settings;
     public GameObject mainMenu;
      public GameObject MAP;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartGame()
    {
        MAP.SetActive(true);
        mainMenu.SetActive(false);
    }
    public void OpenOptions()
    {
        settings.SetActive(true);
        mainMenu.SetActive(false);
    }
    public void CloseOptions()
    {
        settings.SetActive(false);
         mainMenu.SetActive(true);
    }
    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("bye");
    }

     public void ZoomMap()
    {
         
        SceneManager.LoadScene(2,LoadSceneMode.Single);
    }
}
