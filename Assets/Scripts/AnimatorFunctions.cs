using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorFunctions : MonoBehaviour
{
     [SerializeField] menuButtonController MenuButtonController;
     public bool disableOnce;

   void PlaySound(AudioClip whichSound){
    if(!disableOnce){
        MenuButtonController.audioSouce.PlayOneShot(whichSound);
    }else
    {
        disableOnce = false;
    }
   }
}
